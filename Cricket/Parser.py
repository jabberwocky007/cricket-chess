# -*- coding: utf-8 -*-

import re, sys

class Parser(object):

    @staticmethod
    def __init__():
        pass

    def parse_move(self, move):
        # must be organized top to bottom by len(move) (short to long)
        move = move.lower()
        if len(move) == 1 and self.parse_capture(move): # move like 'x' only
            return True
        elif len(move) >= 1 and self.parse_piece(move):
            return True
        elif len(move) >= 2 and self.parse_capture(move[0])\
            and self.parse_piece(move[1:]):
            return True
        elif len(move) >= 2 and len(move[:-1]) <= 3\
            and self.parse_piece(move[:-1])\
            and self.parse_capture(move[-1:]):
            return True
        elif len(move) >= 4 and self.parse_square(move[0:2])\
            and self.parse_square(move[2:]):
            return True
        elif len(move) == 5 and self.parse_square(move[1:3])\
            and self.parse_square(move[3:]):
            return True
        elif len(move) == 4 and (move == 'redo' or move == 'undo'\
            or move == 'help'):
            return True
        elif (len(move) == 2 or len(move) == 3) and self.parse_castling(move):
            return True
        elif 'x' in move:
            m = move.find('x')
            # print('X found at ' + str(m))
            if m == 1:
                capturing = move[0]
                captured = move[2:]
            else:
                capturing = move[:m]
                captured = move[(m+1):]
            # print self.parse_piece(capturing)
            # print self.parse_piece(captured)
            return self.parse_piece(capturing) and self.parse_piece(captured)
        elif len(move) == 1 and move == 'u':
            return True
        else:
            return False

    def parse_piece(self, piece):
        if len(piece) >= 4:
            if self.parse_piece_name(piece[0]) and self.parse_file(piece[1])\
                and self.parse_square(piece[2:]):
                return True
            elif self.parse_piece_name(piece[0]) and self.parse_rank(piece[1])\
                and self.parse_square(piece[2:]):
                return True
            else:
                return False
        elif len(piece) == 3:
            if self.parse_piece_name(piece[0])\
                and self.parse_square(piece[1:]):
                return True
            else:
                return False
        elif len(piece) == 2:
            if self.parse_square(piece):
                return True
            elif self.parse_piece_name(piece[0])\
                and self.parse_file(piece[1:]):
                return True
            elif self.parse_piece_name(piece[0])\
                and self.parse_rank(piece[1:]):
                return True
            else:
                return False
        elif len(piece) < 1:
            return False
        else:
            if self.parse_piece_name(piece):
                return True
            elif self.parse_file(piece):
                return True
            elif self.parse_rank(piece):
                return True
            else:
                return False

    def parse_square(self, square):
        # print("Square: " + square)
        if len(square) == 2:
            return self.parse_file(square[0]) and self.parse_rank(square[1])
        return False

    @staticmethod
    def parse_capture(capture):
        if capture == 'x':
            return True
        else:
            return False

    @staticmethod
    def parse_piece_name(piece_name):
        if re.match(r'[klnpqr]', piece_name) == None:
            return False
        else:
            return True

    @staticmethod
    def parse_file(file_1):
        if re.match(r'[abcdefgh]', file_1) == None:
            return False
        else:
            return True

    @staticmethod
    def parse_rank(rank):
        if re.match(r'[1-8]', rank) == None:
            return False
        else:
            return True

    @staticmethod
    def parse_castling(castling):
        # matches 00, ooo, o0O, OoO, 0-0-0, etc., but not oo-o, 0-Oo, etc.
        if re.match(r'[oO0]{2,3}|[oO0]\-([oO0]\-)?[oO0]', castling) == None:
            return False
        else:
            return True