# -*- coding: utf-8 -*-

def print_figurine_chessboard():
	# R N B Q K B N R
	# P P P P P P P P
	# W B W B W B W B
	# B W B W B W B W
	# W B W B W B W B
	# B W B W B W B W
	# P P P P P P P P
	# R N B Q K B N R
	#
	# White Rook:		\u2656
	# White Knight:		\u2658
	# White Bishop:		\u2657
	# White Queen:		\u2655
	# White King:		\u2654
	# White Pawn:		\u2659
	#
	# Black Rook:		\u265C
	# Black Knight:		\u265E
	# Black Bishop:		\u265D
	# Black Queen:		\u265B
	# Black King:		\u265A
	# Black Pawn:		\u265F
	#
	# Black Square:		\u
	# White Square:		\u
	#
	#print(u"╔══╤═╤═╤═╤═╤═╤═╤══╗\n\
	#		 ║ \u265C│\u265E│\u265D│\u265B│\u265A│\u265D│\u265E│\u265C ║\n\
	#		 ║ \u265F│\u265F│\u265F│\u265F│\u265F│\u265F│\u265F│\u265F ║\n\
	#		 ║┼─┼─┼─┼─┼─┼─┼─┼─┼║\n\
	#		 ║┼─┼─┼─┼─┼─┼─┼─┼─┼║\n\
	#		 ║┼─┼─┼─┼─┼─┼─┼─┼─┼║\n\
	#		 ║┼─┼─┼─┼─┼─┼─┼─┼─┼║\n\
	#		 ║ \u2659│\u2659│\u2659│\u2659│\u2659│\u2659│\u2659│\u2659 ║\n\
	#		 ║ \u2656│\u2658│\u2657│\u2655│\u2654│\u2657│\u2658 \u2656 ║\n\
	#		 ╚══╧═╧═╧═╧═╧═╧═╧══╝")
	print_top_border(0)
	print(u"\
║ \u265C \u265E \u265D \u265B \u265A \u265D \u265E \u265C ║\n\
║ \u265F \u265F \u265F \u265F \u265F \u265F \u265F \u265F ║\n\
║ \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 ║\n\
║ \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 ║\n\
║ \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 ║\n\
║ \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 ║\n\
║ \u2659 \u2659 \u2659 \u2659 \u2659 \u2659 \u2659 \u2659 ║\n\
║ \u2656 \u2658 \u2657 \u2655 \u2654 \u2657 \u2658 \u2656 ║")
	print_bottom_border(0)

def print_notation_chessboard():
	print_top_border(0)
	print(u"\
║ \u0052 \u004E \u0042 \u0051 \u2550 \u265D \u265E \u265C ║\n\
║ \u265F \u265F \u265F \u265F \u265F \u265F \u265F \u265F ║\n\
║ \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 ║\n\
║ \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 ║\n\
║ \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 ║\n\
║ \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 \u2592 \u2591 ║\n\
║ \u2659 \u2659 \u2659 \u2659 \u2659 \u2659 \u2659 \u2659 ║\n\
║ \u0052 \u004E \u0042 \u0051 \u2654 \u2657 \u2658 \u2656 ║")
	print_bottom_border(0)

def print_top_border(top_length):
	if top_length == 0:
		top_length = 18
	bar = u"\u2554" # right corner
	for x in range(1, top_length):
		bar += u"\u2550"
	bar += u"\u2557" # left corner
	print(bar)

def print_bottom_border(bottom_length):
	if bottom_length == 0:
		bottom_length = 18
	bar = u"\u255A" # right corner
	for x in range(1, bottom_length):
		bar += u"\u2550"
	bar += u"\u255D" # left corner
	print(bar)

if __name__ == "__main__":
	print_figurine_chessboard()
	#print_notation_chessboard()

class chess_piece:
	current_pos
	def __init__(cursor):
		current_pos = cursor
	def move():
		print("move")

	def capture():
		print("capture")

	def value():
		print("value")

class white_queen_knight:
	x = chess_piece("c3")
