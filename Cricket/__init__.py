# -*- coding: utf-8 -*-

import re, sys
from Move_Maker import Move_Maker
from Parser import Parser
from Frontend import Frontend

class CricketChess(object):

    mmaker = 0
    parser = Parser()
    front = 0

    chessgame = 1
    last_fen = 0
    half_move = 1
    undo_move_history = []
    redo_move_history = []
    undo_fen_history = [] # tracks fen history for undo purposes
    redo_fen_history = [] # tracks fen redo (will be empty unless
    # undo is called)
    # next move to redo should be next move to pop off (left side)
    fen_counts = {}  # tracks fen history for three-fold repetition

    def get_algebraic_from_literal(self, fullmove, from_sq='', to_sq=''):
        """Converts a literal (g1f3) to algebraic (Nf3)."""
        f = t = 0
        if from_sq == '' and to_sq == '' and len(fullmove) == 4\
            and self.parser.parse_move(fullmove):
            f = fullmove[:2]
            t = fullmove[2:]
        elif self.parser.parse_move(from_sq + to_sq):
            f = from_sq
            t = to_sq
        else:
            return False
        piece = self.mmaker.find_piece_given_square(f)
        if self.mmaker.check_occupied_square(t):
            if piece.lower() == 'p':
                piece = f[0]
                return piece + 'x' + t
            return piece.upper() + 'x' + t
        if piece.lower() == 'p':
            piece = ''
        return piece.upper() + t
        
    def __init__(self, cgame, game_not_over=True):
        self.chessgame = cgame
        self.mmaker = Move_Maker(self.chessgame)
        self.front = Frontend(self.chessgame)
        #Error = ''

        print str(self.chessgame)  # 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'

        print self.chessgame.get_moves()
        """
        ['a2a3', 'a2a4', 'b2b3', 'b2b4', 'c2c3', 'c2c4', 'd2d3', 'd2d4', 'e2e3',
         'e2e4', 'f2f3', 'f2f4', 'g2g3', 'g2g4', 'h2h3', 'h2h4', 'b1c3', 'b1a3',
         'g1h3', 'g1f3']
        """

        #chessgame.apply_move('e2e4')  # succeeds!
        print self.chessgame  # 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1'

        # current_fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
        self.last_fen = str(self.chessgame)
        self.front.clear_screen()
        self.undo_fen_history.append(str(self.chessgame))
        while game_not_over:
            print self.front.print_fen_figurine(self.chessgame, True)
            # print self.get_last_fen()
            legal_move = False
            while legal_move == False:
            #try: # comment this line out for debug mode
                move = self.front.get_user_move(self.mmaker.whosturn())
                # print move
                legal_move = True
                if move.lower() == 'u' or move.lower() == 'undo':
                    self.front.clear_screen()
                    if len(self.undo_move_history) == 0 or len(self.undo_fen_history) == 0:
                        print 'There are no moves to undo.'
                    else:
                        item = str(self.chessgame)[:-4]
                        self.fen_counts[item] -= 1
                        self.redo_move_history.append(self.undo_move_history.pop())
                        float_fen = self.undo_fen_history.pop()
                        self.redo_fen_history.append(str(self.chessgame))
                        self.chessgame.set_fen(float_fen)
                elif move.lower() == 'redo':
                    self.front.clear_screen()
                    if len(self.redo_move_history) == 0 or len(self.redo_fen_history) == 0:
                        print 'There are no moves to redo.'
                    else:
                        self.undo_move_history.append(self.redo_move_history.pop())
                        float_fen = self.redo_fen_history.pop()
                        self.undo_fen_history.append(str(self.chessgame))
                        self.chessgame.set_fen(float_fen)
                        item = str(self.chessgame)[:-4]
                        self.fen_counts[item] += 1
                elif move.lower() == 'clear' or move.lower() == 'cls':
                    self.front.clear_screen()
                elif move.lower() == 'exit':
                    self.front.clear_screen()
                    game_not_over = False # game is now over
                elif move.lower() == 'export':
                    self.front.export_pgn()
                    game_not_over = False
                elif move.lower() == 'import':
                    pass #import a fen or pgn
                elif move.lower() == 'print':
                    self.front.clear_screen()
                    print self.chessgame
                else:
                    mm = self.mmaker.make_move(move)
                    if type(mm) == str and len(mm) == 4:
                        print('M&M', mm)
                        self.front.clear_screen()
                        self.undo_move_history.append(self.get_algebraic_from_literal(mm))
                        self.undo_fen_history.append(str(self.chessgame))
                        self.chessgame.apply_move(mm)
                        if len(self.redo_fen_history) != 0:
                            del self.redo_fen_history[:]
                        if len(self.redo_move_history) != 0:
                            del self.redo_move_history[:]
                        item = str(self.chessgame)[:-4]
                        if self.fen_counts.has_key(item):
                            self.fen_counts[item] += 1
                            if self.fen_counts[item] == 3:
                                print self.front.print_fen_figurine(self.chessgame, True)
                                print 'Gameover by three-fold repetition.'
                                game_not_over = False
                        else:
                            self.fen_counts[item] = 1
                        #print self.fen_counts
                        
                        if self.chessgame.status == 0:
                            pass
                        elif self.chessgame.status == 1:
                            print 'Check!'
                        elif self.chessgame.status == 2:
                            print self.front.print_fen_figurine(self.chessgame, True)
                            print 'Checkmate.'
                            if self.mmaker.whosturn == 'w':
                                print 'White wins.'
                            else:
                                print 'Black wins.'
                            game_not_over = False
                        elif self.chessgame.status == 3:
                            print self.front.print_fen_figurine(self.chessgame, True)
                            print 'Stalemate.'
                            game_not_over = False
                        else:
                            pass

                        # self.front.clear_screen()
                        self.half_move += 1
                    elif mm == True:
                        self.front.clear_screen()
                        print 'This move is not implemented yet.', move, '\n'
                    else:
                        self.front.clear_screen()
                        print 'This move results in an error:', move, '\n'
                #print self.chessgame
                #print self.undo_move_history
                #print self.undo_fen_history
                #print self.redo_move_history
                #print self.redo_fen_history
                #self.last_fen = current_fen
                #current_fen = str(self.chessgame)
                    
            #except: # comment this line out for debug mode
                #print 'Illegal move.'
                #legal_move = False

    
