# -*- coding: utf-8 -*-

import re, sys
from Parser import Parser
	
class Frontend(object):

    parser = Parser()
    chessgame = 0

    def __init__(self, top_chessgame):
        self.chessgame = top_chessgame

    @staticmethod
    def print_top_border(top_length, coords):
        """Prints the top bar of the figurine chessboard."""
        if top_length == 0:
            top_length = 18
        top_bar = ""
        if coords:
            top_bar += "  "
        top_bar += u"\u2554" # right corner
        for _ in range(1, top_length):
            top_bar += u"\u2550"
        top_bar += u"\u2557" # left corner
        return top_bar

    @staticmethod
    def print_bottom_border(bottom_length, coords):
        """Prints the bottom bar of the figurine chessboard."""
        if bottom_length == 0:
            bottom_length = 18
        bot_bar = ""
        if coords:
            bot_bar += "  "
        bot_bar += u"\u255A" # right corner
        for _ in range(1, bottom_length):
            bot_bar += u"\u2550"
        bot_bar += u"\u255D" # left corner
        if coords:
            bot_bar += '\n'
            bot_bar += '    a b c d e f g h  '
        return bot_bar

    def print_fen_figurine(self, fen, coords):
        """Prints the chessboard with figurines."""
        rows = str(fen).split("/")
        row8 = rows[7].split()
        rows[7] = row8[0]

        output = self.print_top_border(0, coords) + '\n'

        for row_num in range(0, 8):
            if coords:
                output += str(8-row_num) + ' '
            output += u'║'
            current = rows[row_num]
            bw = self.convert_bw_squares(current)
            result = self.convert_boring_to_exciting(bw, row_num)
            output += self.convert_notation_utf8_figurine(result)
            output += u'║'
            if row_num != 7:
                output += '\n'

        return output + '\n' + self.print_bottom_border(0, coords)

    @staticmethod
    def expand_board(squares_ls):
        """Changes the consolidated FEN format to a more liberal display."""
        new_squares = []
        for character in squares_ls:
            new_squares.append(' ')
            new_squares.append(character)
        new_squares.append(' ')
        return "".join(new_squares)

    @staticmethod
    def convert_bw_squares(text):
        """Converts numbers in a FEN to a string of the same number 
        of blank characters (tildes, actually).
        """
        text = re.sub('1', "~", text)
        text = re.sub('2', "~~", text)
        text = re.sub('3', "~~~", text)
        text = re.sub('4', "~~~~", text)
        text = re.sub('5', "~~~~~", text)
        text = re.sub('6', "~~~~~~", text)
        text = re.sub('7', "~~~~~~~", text)
        text = re.sub('8', "~~~~~~~~", text)
        return text

    def convert_boring_to_exciting(self, text, row_num):
        """Changes squares to unicode boxes to look like a checkerboard."""
        squares = list(text)
        for char_index in xrange(0, 8):
            if squares[char_index] == '~':
                if ( char_index + row_num ) % 2 == 0:
                    squares[char_index] = ' ' # u'\u2591'
                else:
                    squares[char_index] = u'\u2591' # u'\u2592'
        return self.expand_board(squares)

    @staticmethod
    def convert_notation_utf8_figurine(text):
        """Converts letters to the unicode chess piece."""
        text = re.sub('r', u"\u265C", text)
        text = re.sub('n', u"\u265E", text)
        text = re.sub('b', u"\u265D", text)
        text = re.sub('q', u"\u265B", text)
        text = re.sub('k', u"\u265A", text)
        text = re.sub('p', u"\u265F", text)
        text = re.sub('R', u"\u2656", text)
        text = re.sub('N', u"\u2658", text)
        text = re.sub('B', u"\u2657", text)
        text = re.sub('Q', u"\u2655", text)
        text = re.sub('K', u"\u2654", text)
        text = re.sub('P', u"\u2659", text)
        return text

    #@staticmethod
    #def convert_piece_move_to_literal_move(piece, move):
    #    return ''

    @staticmethod
    def convert_letter_to_number(letter):
        """Conversion from a file letter to a number. a=1, h=8"""
        result = False
        if letter == 'a':
            result = 1
        elif letter == 'b':
            result = 2
        elif letter == 'c':
            result = 3
        elif letter == 'd':
            result = 4
        elif letter == 'e':
            result = 5
        elif letter == 'f':
            result = 6
        elif letter == 'g':
            result = 7
        elif letter == 'h':
            result = 8
        else:
            pass
        return result

    @staticmethod
    def convert_number_to_letter(number):
        """Conversion from a number to the corresponding file letter. 8=h"""
        if number < 9 and number > 0:
            return 'abcdefgh'[number - 1]
        else:
            return False

    @staticmethod
    # number is per side (don't forget pawns can upgrade)
    def possible_max_piece(piece_letter):
        """Returns the max number one side can have of a given piece."""
        result = 0
        if piece_letter == 'k':
            result = 1
        elif piece_letter == 'l':
            result = 10
        elif piece_letter == 'n':
            result = 10
        elif piece_letter == 'p':
            result = 8
        elif piece_letter == 'q':
            result = 9
        elif piece_letter == 'r':
            result = 10
        else:
            pass
        return result

    @staticmethod
    def clear_screen():
        """On Linux, clears console screen."""
        print '\n' * 100
        print "\033[H\033[J"
        return

    def get_user_move(self, whosturn):
        """Asks user for input and handles it."""
        user_move = 0
        parse_bool = False
        while parse_bool == False:
            if user_move != 0:
                self.clear_screen()
                print self.print_fen_figurine(self.chessgame, True)
                print "That's not a move. (Incorrect syntax)"
            if whosturn == 'w':
                user_move = raw_input('White to move: ').lower()
            else:
                user_move = raw_input('Black to move: ').lower()
            if user_move.lower() == 'clear'\
                or user_move.lower() == 'cls'\
                or user_move.lower() == 'exit'\
                or user_move.lower() == 'export'\
                or user_move.lower() == 'import'\
                or user_move.lower() == 'print'\
                or user_move.lower() == 'help':
                return user_move
            parse_bool = self.parser.parse_move(user_move)
            
        print "Correct syntax."
        return user_move

    def export_pgn(self):
        """Writes the chess game to a pgn file."""
        f       = raw_input('Filename: ')
        f_hand  = open(f+'.pgn', 'w')
        event   = raw_input('Event: ')
        site    = raw_input('Site: ')
        date    = raw_input('Date: ')
        round_n = raw_input('Round Number: ')
        white_p = raw_input('White Player: ')
        black_p = raw_input('Black Player: ')
        result  = raw_input('Result: ')
        final = ''
        final += '[Event "'  + event   + '"]\n'
        final += '[Site "'   + site    + '"]\n'
        final += '[Date "'   + date    + '"]\n'
        final += '[Round "'  + round_n + '"]\n'
        final += '[White "'  + white_p + '"]\n'
        final += '[Black "'  + black_p + '"]\n'
        final += '[Result "' + result  + '"]\n'
        final += '\n'
        n = 0
        for item in self.undo_move_history:
            n += 1
            if n % 2:
                final += str(n/2 + 1) + '. '
            final += str(item)
            final += ' '
        final += result
        print final
        f_hand.write(final)