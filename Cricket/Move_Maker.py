# -*- coding: utf-8 -*-

import re, sys
from Parser import Parser
from Frontend import Frontend

class Move_Maker(object):

    parser = Parser()
    chessgame = 0

    def __init__(self, top_chessgame):
        self.chessgame = top_chessgame

    def make_move(self, move):
        move = move.lower()
        if len(move) == 1 and self.parser.parse_capture(move):
            return move # most involved; requires finding both squares
            # aka, iterate through each piece and find the capture and
            # if more than one, fail out
        elif len(move) >= 1 and self.parser.parse_piece(move):
            return self.make_piece(move, 0)
        elif len(move) >= 2 and self.parser.parse_capture(move[0])\
            and self.parser.parse_piece(move[1:]):
            return move
        elif len(move) >= 2 and self.parser.parse_piece(move[:-1])\
            and self.parser.parse_capture(move[-1:]):
            return False
            return self.make_piece(move[:-1]) # TODO: fill this out
        elif len(move) >= 4 and self.parser.parse_square(move[0:2])\
            and self.parser.parse_square(move[2:]):
            return move[0:2] + move[2:]
        elif len(move) == 5 and self.parser.parse_square(move[1:3])\
            and self.parser.parse_square(move[3:]):
            return move[1:3] + move[3:]
        elif (len(move) == 2 or len(move) == 3)\
            and self.parser.parse_castling(move):
            return self.make_castling(move)
        elif 'x' in move:
            m = move.find('x')
            # print('X found at ' + str(m))
            if m == 1:
                capturing = move[0]
                captured = move[2:]
            else:
                capturing = move[:(m-1)]
                captured = move[(m+1):]
            # print capturing
            # print captured
            if self.parser.parse_piece(capturing) != True\
                and self.parser.parse_piece(captured) != True:
                return False
            for n1 in xrange(0, 8):
                cpg = self.make_piece(capturing, n1)
                if cpg != False:
                    for n2 in xrange(0, 8):
                        cpd = self.make_piece(captured, n2, True)
                        if cpd != False:
                            print 'carl: \t' + str(capturing) + '\t'\
                                + str(cpg) + str(cpd) + '\t' + str(captured)
                            if self.can_move_here(capturing, cpg, cpd, True):
                                return cpg + cpd
            return False
            # TODO: this while is unreachable?
            n = 0
            while self.can_move_here(capturing, cpg, cpd, True) == False:
                n += 1
                cpg = self.make_piece(capturing, n)
                cpd = self.make_piece(captured, n)
                if n > 8:
                    return False
            return cpg + cpd
        elif len(move) == 1 and move.lower() == 'u':
            # self.chessgame.set_fen(self.get_last_fen())
            return False # TODO: handle undo
        else:
            new_move = move
            # print(find_square_piece_is_on(chessgame, 'N'))
            # print(find_piece_given_square(chessgame, 'g4'))
            return new_move

    def make_piece(self, piece, nexter, is_captured=False):
        if len(piece) >= 4: # works, but needs testing
            if self.parser.parse_piece_name(piece[0])\
                and self.parser.parse_file(piece[1])\
                and self.parser.parse_square(piece[2:]):
                n = 0
                from_square = self.make_piece_name(piece[0], n, is_captured)
                while from_square\
                    and from_square[0] != self.make_file(piece[1]):
                    from_square = \
                        self.make_piece_name(piece[0], n, is_captured)
                    n += 1
                if from_square != False:
                    return from_square + piece[2:]
                    # TODO: works, but needs testing
                return False
            elif self.parser.parse_piece_name(piece[0])\
                and self.parser.parse_rank(piece[1])\
                and self.parser.parse_square(piece[2:]):
                n = 0
                from_square = self.make_piece_name(piece[0], n)
                while from_square\
                    and from_square[1] != self.make_rank(piece[1]):
                    from_square = \
                        self.make_piece_name(piece[0], n, is_captured)
                    n += 1
                if from_square != False:
                    return from_square + piece[2:] # works, but needs testing
                return False
            else:
                return False
        elif len(piece) == 3:
            if self.parser.parse_piece_name(piece[0])\
                and self.parser.parse_square(piece[1:]):
                n = 0
                from_square = self.make_piece_name(piece[0], n, is_captured)
                if from_square != False:
            # print '>> ' + str(piece[0])+str(from_square)+str(piece[1:])
                    while self.can_move_here(piece[0], \
                        from_square, piece[1:], is_captured) != True:
                        n += 1
                        from_square = \
                            self.make_piece_name(piece[0], n, is_captured)
                        # print('Froth: ', from_square)
                        if n > 8:
                            return False
                        # print('....returning', from_square + piece[1:], \
                        #    self.can_move_here(piece[0], from_square, \
                        #        piece[1:], is_captured))
                    if self.can_move_here(piece[0],
                        from_square, piece[1:], is_captured):
                        # print '^^^^^^^^^^^^^^^^^^'
                        return from_square + piece[1:] # must check if selected piece can # TODO: line too long
                    # move to the square and for multiple pieces being able to            # TODO: line too long
                    # move to the same square<---May 8, What? the piece is defined in piece[0] # TODO: line too long
                return False
            else:
                return False
        elif len(piece) == 2: # At this point, we don't believe piece is a piece, but a square. # TODO: line too long
        # UPDATE: But it could be piece_name and file or rank!
            if self.parser.parse_square(piece):
                sq = self.make_square(piece)
                n = 0
                #new_p = 'p' # A move like: e4 is presently considered a pawn move. # TODO: line too long
                # Should now iterate through pieces to find the piece that should move to # TODO: line too long
                # a square. Will need (TODO:) tweaking, but not twerking 
                new_piece = 'pkqrln'
                j = 0
                from_square = \
                    self.make_piece_name(new_piece[j], n, is_captured)
                if from_square != False:
                    while self.can_move_here(new_piece[j], from_square, \
                        sq, is_captured) != True:
                        n += 1
                        max_n = self.number_of_this_piece(new_piece[j])
                        from_square = \
                            self.make_piece_name(new_piece[j], n, is_captured)
                        # print('Suggested move:', new_piece[j], from_square)
                        if n > max_n and j < 5:
                            n = 0
                            j += 1
                        elif n > max_n and j >= 5:
                            return False
                        else:
                            pass
                    return from_square + sq
                return False
            elif self.parser.parse_piece_name(piece[0]) and self.parser.parse_file(piece[1:]):
                n = 0
                sq = self.make_piece_name(piece[0], n, is_captured)
                while sq and sq[0] != Frontend.convert_letter_to_number(self.make_file(piece[1])):
                    sq = self.make_piece_name(piece[0], n, is_captured)
                    n += 1
                    if n > 8:
                        return False
                return True
            elif self.parser.parse_piece_name(piece[0]) and self.parser.parse_rank(piece[1:]):
                n = 0
                sq = self.make_piece_name(piece[0], n, is_captured)
                while sq and sq[1] != self.make_rank(piece[1]):
                    sq = self.make_piece_name(piece[0], n, is_captured)
                    n += 1
                    if n > 8:
                        return False
                return True
            else:
                return False
        elif len(piece) < 1:
            return False
        else:
            if self.parser.parse_piece_name(piece):
                nexx = nexter
                result = self.make_piece_name(piece, nexx, is_captured)
                # print 'Result: ' + str(result)
                while result == False:
                    nexx += 1
                    result = self.make_piece_name(piece, nexx, is_captured)
                    if nexx > 8 and result == False:
                        return False
                return result
            elif self.parser.parse_file(piece): # TODO: fill out?
                return True
            elif self.parser.parse_rank(piece): # TODO: fill out?
                return True
            else:
                return False

    def make_piece_name(self, piece_name, n, is_opponent_piece=False):
        if re.match(r'[klnpqr]', piece_name) == None:
            print '"' + piece_name + '" is not a name for a piece'
            return False
        else:
            sq = self.find_square_piece_is_on(piece_name, n, is_opponent_piece)
            try:
                # print 'sq: ' + str(sq[n])
                if sq[n]:
                    return sq[n]
                else:
                    return sq.pop() # TODO: I don't like this block of code.
            except:
                return sq.pop()

    def make_square(self, square):
        return self.make_file(square[0]) + self.make_rank(square[1])

    @staticmethod
    def make_capture(capture):
    # TODO: Is this needed? Var 'capture' should always be 'x'
    # UPDATE: See notes. Capture is sometime designated Like 'Ne4:'
        return False

    @staticmethod
    def make_file(file_1):
        if re.match(r'[abcdefgh]', file_1) == None:
            return False
        else:
            return file_1

    @staticmethod
    def make_rank(rank):
        if re.match(r'[1-8]', rank) == None:
            return False
        else:
            return rank

    def make_castling(self, move):
        move = re.sub(r'-', '', move)
        turn = self.whosturn()
        if turn == 'w':
            n = '1'
        else:
            n = '8'
        if move == 'oo' or move == '00':
            if self.check_clear_rank(n, 5, 8):
                return 'e' + n + 'g' + n
        elif move == 'ooo' or move == '000':
            if self.check_clear_rank(n, 5, 3):
                return 'e' + n + 'c' + n
        else:
            return False

    # if the board was blank
    def can_move_here(self, piece_n, from_sq, to_sq, capture=False):
        """Returns a boolean if a given piece can move to a given square."""
        if self.can_this_color_move_here(to_sq, capture) == False:
            return False
        piece_name = piece_n.lower()
        turn = self.whosturn()
        from_sqn0 = Frontend.convert_letter_to_number(from_sq[0])
        from_sqn1 = int(from_sq[1])
        to_sqn0 = Frontend.convert_letter_to_number(to_sq[0])
        to_sqn1 = int(to_sq[1])
        if piece_name == 'p':
            if turn == 'w':
                if to_sqn1 == from_sqn1 + 1:
                    if to_sqn0 == from_sqn0 and capture == False:
                        return True
                    elif capture and to_sqn0 == from_sqn0 - 1 or to_sqn0 == from_sqn0 + 1:
                        if to_sqn0 <= 8 and to_sqn0 >= 1 and to_sqn1 <= 8 and to_sqn1 > 2:
                            if self.check_occupied_square(to_sq): # TODO: need to handle en passant from F.E.N.
                                return True
                elif capture == False\
                    and from_sqn1 == 2 and to_sqn1 == 4\
                    and not self.check_occupied_square(to_sq[0]+'3'):
                    if to_sqn0 == from_sqn0:
                        return True
                else:
                    return False
                return False
            if turn == 'b':
                # print('From: ', from_sqn0, from_sqn1, ' To: ', to_sqn0, to_sqn1)
                if to_sqn1 == from_sqn1 - 1:
                    if to_sqn0 == from_sqn0 and capture == False:
                        return True
                    elif capture and to_sqn0 == from_sqn0 - 1\
                        or to_sqn0 == from_sqn0 + 1:
                        if to_sqn0 <= 8 and to_sqn0 >= 1 and to_sqn1 >= 1\
                            and to_sqn1 < 7:
                            if self.check_occupied_square(to_sq):
                                return True
                elif capture == False\
                    and from_sqn1 == 7 and to_sqn1 == 5\
                    and not self.check_occupied_square(to_sq[0]+'6'):
                    if to_sqn0 == from_sqn0:
                        return True
                else:
                    return False
                return False
        elif piece_name == 'l':
            if to_sqn0 >= 1 and to_sqn0 <= 8 and to_sqn1 >= 1 and to_sqn1 <= 8:
                dfx = abs(to_sqn0 - from_sqn0)
                dfy = abs(to_sqn1 - from_sqn1)
                if dfx == dfy:
                    if dfx < 8 and dfx > 0:
                        if dfy < 8 and dfy > 0:
                            if self.check_clear_diagonal(from_sqn0, \
                                from_sqn1, to_sqn0, to_sqn1):
                                return True
            return False
        elif piece_name == 'n':
            if to_sqn0 >= 1 and to_sqn0 <= 8 and to_sqn1 >= 1 and to_sqn1 <= 8:
                if to_sqn0 == from_sqn0 + 1:
                    if to_sqn1 == from_sqn1 + 2: # %
                        return True
                    if to_sqn1 == from_sqn1 - 2: # &
                        return True
                elif to_sqn0 == from_sqn0 - 1:
                    if to_sqn1 == from_sqn1 + 2: # ^
                        return True
                    if to_sqn1 == from_sqn1 - 2: # !
                        return True
                elif to_sqn0 == from_sqn0 + 2:
                    if to_sqn1 == from_sqn1 + 1: # =
                        return True
                    if to_sqn1 == from_sqn1 - 1: # +
                        return True
                elif to_sqn0 == from_sqn0 - 2:
                    if to_sqn1 == from_sqn1 + 1: # $
                        return True
                    if to_sqn1 == from_sqn1 - 1: # @
                        return True
                else:
                    # print 'Fr:'+str(from_sqn0)+str(from_sqn1)+' To:'+str(to_sqn0)+str(to_sqn1)
                    return False
            return False
            #   ╔═════════════════╗
            # 8 ║   ░   ░   ░   ░ ║
            # 7 ║ ░   ░ + ░ = ░   ║
            # 6 ║   ░ & ░   ░ % ░ ║
            # 5 ║ ░   ░   N   ░   ║
            # 4 ║   ░ ! ░   ░ ^ ░ ║
            # 3 ║ ░   ░ @ ░ $ ░   ║
            # 2 ║   ░   ░   ░   ░ ║
            # 1 ║ ░   ░   ░   ░   ║
            #   ╚═════════════════╝
        elif piece_name == 'r':
            if to_sqn0 >= 1 and to_sqn0 <= 8 and to_sqn1 >= 1 and to_sqn1 <= 8:
                dfx = abs(to_sqn0 - from_sqn0)
                dfy = abs(to_sqn1 - from_sqn1)
                if dfx < 8 and dfx > 0 and dfy == 0:
                    if dfx == 1:
                        return True # Can skip checking clear rank because
                                    # piece has only moved one space.
                    if self.check_clear_rank(from_sqn1, from_sqn0, to_sqn0):
                        return True
                if dfy < 8 and dfy > 0 and dfx == 0:
                    if dfy == 1:
                        return True # Can skip checking clear file because
                                    # piece has only moved one space.
                    if self.check_clear_file(from_sq[0], from_sqn1, to_sqn1):
                        return True
            return False
        elif piece_name == 'k':
            if to_sqn0 >= 1 and to_sqn0 <= 8 and to_sqn1 >= 1 and to_sqn1 <= 8:
                dfx = abs(to_sqn0 - from_sqn0)
                dfy = abs(to_sqn1 - from_sqn1)
                if dfy == 1:
                    if dfx == 1 or dfx == 0:
                        return True
                if dfy == 0:
                    if dfx == 1:
                        return True
            return False
        elif piece_name == 'q':
            if to_sqn0 >= 1 and to_sqn0 <= 8 and to_sqn1 >= 1 and to_sqn1 <= 8:
                dfx = abs(to_sqn0 - from_sqn0)
                dfy = abs(to_sqn1 - from_sqn1)
                if dfx == dfy:
                    if dfx < 8 and dfx > 0:
                        if dfy < 8 and dfy > 0:
                            if self.check_clear_diagonal(from_sqn0, \
                                from_sqn1, to_sqn0, to_sqn1):
                                return True
                elif dfx < 8 and dfx > 0 and dfy == 0:
                    if dfx == 1:
                        return True # Can skip checking clear rank because
                                    # piece has only moved one space.
                    if self.check_clear_rank(from_sqn1, from_sqn0, to_sqn0):
                        return True
                elif dfy < 8 and dfy > 0 and dfx == 0:
                    if dfy == 1:
                        return True # Can skip checking clear file because
                                    # piece has only moved one space.
                    if self.check_clear_file(from_sq[0], from_sqn1, to_sqn1):
                        return True
                else:
                    return False
            return False
        else:
            return False

    def get_last_fen(self):
        return self.last_fen

    def number_of_this_piece(self, piece_letter):
        """Returns the number one side has of a given piece."""
        fen = str(self.chessgame)
        rows = fen.split("/")
        row8 = rows[7].split()
        rows[7] = row8[0]
        n = 0
        for row in rows:
            for piece in row:
                if self.piece_nicknames(piece) == piece_letter:
                    n += 1
        return n

    def whosturn(self):
        """Returns as a letter who's turn it is. w/b"""
        # could also be:
        # return str(self.chessgame).split("/")[7].split()[0][1]
        fen = str(self.chessgame)
        rows = fen.split("/")
        row8 = rows[7].split()
        rows[7] = row8[0]
        turn = row8[1]
        return turn

    @staticmethod
    def piece_nicknames(text): 
        """Assigns a nickname to certain pieces for input purposes. bishop=l"""
    # defines nicknames for given pieces
    # Presently, only bishop is mapped to a nickname; all other 
    # piece names are identical to what I would name them.
    # In French, the bishops are indicated by F, rather than B.
    # I'm using 'L'.
        text = re.sub('b', 'l', text)
        text = re.sub('B', 'L', text)
        return text

    def find_square_piece_is_on(self, piece_n,
        next_inc, is_opponent_piece=False):
        """Returns square for a given piece name."""
        piece_name = piece_n.lower()
        if self.whosturn() == 'w':
            if is_opponent_piece == False:
                piece_name = piece_n.upper()
        else:
            if is_opponent_piece:
                piece_name = piece_n.upper()
        fen = str(self.chessgame)
        rows = fen.split("/")
        row8 = rows[7].split()
        rows[7] = row8[0]
        squares_ls = []
        n = 9
        for row_old in rows:
            n -= 1
            row_new = Frontend.convert_bw_squares(row_old)
            for a in xrange(0, 8):
            # xrange goes to one less than max (i.e. 7 here)
                if self.piece_nicknames(row_new[a]) == piece_name:
                    ls = Frontend.convert_number_to_letter(a+1) + str(n)
                    squares_ls.append(ls)
        # print('@:', squares_ls)
        return squares_ls

    def find_piece_given_square(self, square):
        """Returns the piece residing on a given square."""
        fen = str(self.chessgame)
        rows = fen.split("/")
        row8 = rows[7].split()
        rows[7] = row8[0]
        addr_file = Frontend.convert_letter_to_number(square[0])
        # print('+++SQ: ', square)
        addr_rank = int(square[1])
        # print('+++QQ: ', rows[8-addr_rank])
        piece = Frontend.convert_bw_squares(rows[8-addr_rank])[addr_file-1]
        return piece

    # Note: Does not check that ending square is unoccupied.
    # Test scenario: file_letter=g, from_r=7, to_r=5
    def check_clear_file(self, file_letter, from_r, to_r): # column
        """Returns a boolean for whether a span of a file is clear or pieces.
        """
        start = end = 0
        if from_r +1 == to_r or to_r + 1 == from_r:
            return True
        elif from_r > to_r:     # 7 > 5
            start = to_r + 1    # 6
            end = from_r        # 7
        elif to_r > from_r:
            start = from_r + 1
            end = to_r
        else:
            return True
        for sq in xrange(start, end): # 6, 7, one square will be checked (g6)
            if self.check_occupied_square(file_letter+str(sq)):
                return False
        return True

    # Note: Does not check that ending square is unoccupied.
    # Test scenario: rank=8, from_f=h or 8, to_f=a or 1
    def check_clear_rank(self, rank, from_f, to_f): # row; checks all squares in between
        """Returns a boolean for whether a span of a rank is clear of pieces.
        """
        start = end = 0
        if from_f + 1 == to_f or to_f + 1 == from_f:
            return True
        elif from_f > to_f:     # 8 > 1
            start = to_f + 1    # 2
            end = from_f        # 8
        elif to_f > from_f:
            start = from_f + 1
            end = to_f
        else:
            return True
        for sq in xrange(start, end): # 2, 8
            fl = Frontend.convert_number_to_letter(sq)
            if self.check_occupied_square(fl+str(rank)):
                return False
        return True

    @staticmethod
    def ls_generate(from_n, to_n):
        """Generates a list of numbers in between the two arguments 
        regardless which is larger.
        """
        n = 0
        if from_n > to_n:
            n = -1
        else:
            n = 1
        return range(from_n + n, to_n, n)

    def check_clear_diagonal(self, from_sqn0, from_sqn1, to_sqn0, to_sqn1):
        """Returns a boolean for whether a span of a diagonal is clear of 
        pieces.
        """
        file_ls = list(self.ls_generate(from_sqn0, to_sqn0))
        rank_ls = list(self.ls_generate(from_sqn1, to_sqn1))
        k = 0
        for j in file_ls:
            print j
            a = Frontend.convert_number_to_letter(int(j))
            b = rank_ls[k]
            if self.check_occupied_square(str(a) + str(b)):
                return False
            k += 1
        return True

    def check_occupied_square(self, square): # returns True
        """Returns a boolean if a square is occupied."""
        if self.find_piece_given_square(square) == '~':
            return False
        else:
            return True

    def can_this_color_move_here(self, square, capture=False):
        """Returns a boolean if current player can move to a given square."""
        # print('can_color-->', square)
        piece_reside = self.find_piece_given_square(square)
        if piece_reside == '~':
            # print('can_color-->can')
            return True
        # print('can_color-->cannot-->', piece_reside)
        turn = self.whosturn()
        if turn == 'b':
            if re.match(r'[klnpqr]', piece_reside) == None and capture:
                return True
            return False
        elif turn == 'w':   # I prefer a literal elif and then use else
                            # rather than just an else, even though I
                            # could use that.
            if re.match(r'[KLNPQR]', piece_reside) == None and capture:
                return True
            return False
        else: 
            return False