# -*- coding: utf-8 -*-

import re, sys

# Beginning in September, I changed my mind how the parser handles
# returning for two reasons:
# 1) Functions are best written and easiest to debug with one return
# statement
# 2) Given an integer return, I immediately know what move I am
# dealing with
#
# Integer 0 represents a false move
# Integers 1-127 represent specific moves and they can build:
# --> e.g. Move 8 by itself is a capture 'x'
# --> yet, move 112 is a piece (R), and its square (b5), or Rb5
# --> so, move 120 is Rb5x
# Clever, eh?
#
# Move 128 will represent castling
# Move 333 represents other valid inputs, but not really moves

class Parser(object):

    @staticmethod
    def __init__():
        pass

    def parse_move(self, move):
        # must be organized top to bottom by len(move) (short to long)
        move = move.lower()
        if len(move) == 1 and self.parse_capture(move) > 0: # move like 'x' only
            return self.parse_capture(move)
            # Returns self.parse_capture(move) i.e. 8
        elif len(move) >= 1 and self.parse_piece(move) > 0:
            return self.parse_piece(move)
            # i.e. (see parse_piece for specifics)
        elif len(move) >= 2 and self.parse_capture(move[0]) > 0\
            and self.parse_piece(move[1:]) > 0:
            return self.parse_capture(move[0]) +\
                self.parse_piece(move[1:])
                # i.e. 8 + parse_piece
                # e.g. 'x' + 'Rba6' = 'Rbxa6'
                # -->   8  +   99   =   107
        elif len(move) >= 2 and len(move) < 4\
            and self.parse_piece(move[:-1]) > 0\
            and self.parse_capture(move[-1:]) > 0:
            return self.parse_piece(move[:-1]) * 16 +\
                self.parse_capture(move[-1:])
                # i.e. Rb5x
        elif len(move) == 4 and self.parse_square(move[0:2]) > 0\
            and self.parse_square(move[2:]) > 0:
            return 51
        elif len(move) == 5 and self.parse_piece_name(move[0]) > 0\
            and self.parse_square(move[1:3]) > 0\
            and self.parse_square(move[3:]) > 0:
            return 115
        elif len(move) == 4 and (move == 'redo' or move == 'undo'\
            or move == 'help'):
            return 333
        elif (len(move) == 2 or len(move) == 3) and self.parse_castling(move):
            return 128
        elif 'x' in move:
            m = move.find('x')
            # print('X found at ' + str(m))
            if m == 1:
                capturing = move[0]
                captured = move[2:]
            else:
                capturing = move[:m]
                captured = move[(m+1):]
            # print self.parse_piece(capturing)
            # print self.parse_piece(captured)
            # print '-- Capturing: ' + capturing
            # print '-- Captured:  ' + captured
            if len(capturing) <= 3\
                and len(captured) <= 3\
                and self.parse_piece(capturing) > 0\
                and self.parse_piece(captured) > 0:
                return self.parse_piece(capturing) * 16 +\
                    self.parse_piece(captured) + 8
            elif len(capturing) <= 3\
                and len(captured) == 0\
                and self.parse_piece(capturing) > 0:
                return self.parse_piece(capturing) * 16 + 8
            else:
                return 0
        elif len(move) == 1 and move == 'u':
            return 333
        else:
            return 0

    def parse_piece(self, piece):
        if len(piece) >= 4:
            if self.parse_piece_name(piece[0]) > 0\
                and self.parse_file(piece[1]) > 0\
                and self.parse_square(piece[2:]) > 0:
                return self.parse_piece_name(piece[0]) * 16 +\
                    self.parse_file(piece[1]) * 16 +\
                    self.parse_square(piece[2:])
                    # i.e. 99
            elif self.parse_piece_name(piece[0]) > 0\
                and self.parse_rank(piece[1]) > 0\
                and self.parse_square(piece[2:]) > 0:
                return self.parse_piece_name(piece[0]) * 16 +\
                    self.parse_rank(piece[1]) * 16 +\
                    self.parse_square(piece[2:])
                    # i.e. 83
            else:
                return 0
        elif len(piece) == 3:
            if self.parse_piece_name(piece[0]) > 0\
                and self.parse_square(piece[1:]) > 0:
                return self.parse_piece_name(piece[0]) +\
                    self.parse_square(piece[1:])
                    # i.e. 7 (-Na6)
            else:
                return 0
        elif len(piece) == 2:
            if self.parse_square(piece) > 0:
                return self.parse_square(piece)
                # i.e. 3
            elif self.parse_piece_name(piece[0]) > 0\
                and self.parse_file(piece[1:]) > 0:
                return self.parse_piece_name(piece[0]) +\
                    self.parse_file(piece[1:])
                    # i.e. 6
            elif self.parse_piece_name(piece[0]) > 0\
                and self.parse_rank(piece[1:]) > 0:
                return self.parse_piece_name(piece[0]) +\
                    self.parse_rank(piece[1:])
                    # i.e. 5
            else:
                return 0
        elif len(piece) < 1:
            return 0
        else:
            if self.parse_piece_name(piece) > 0:
                return self.parse_piece_name(piece)
                # i.e. 4
            elif self.parse_file(piece) > 0:
                return self.parse_file(piece)
                # i.e. 1
            elif self.parse_rank(piece) > 0:
                return self.parse_rank(piece)
                # i.e. 2
            else:
                return 0

    def parse_square(self, square):
        # print("Square: " + square)
        if len(square) == 2\
            and self.parse_file(square[0]) > 0\
            and self.parse_rank(square[1]) > 0:
            return self.parse_file(square[0]) +\
                self.parse_rank(square[1]) # i.e. 3
        else:
            return 0

    @staticmethod
    def parse_capture(capture):
        if capture == 'x':
            return 8
        else:
            return 0

    @staticmethod
    def parse_piece_name(piece_name):
        if re.match(r'[klnpqr]', piece_name) == None:
            return 0
        else:
            return 4

    @staticmethod
    def parse_file(file_1):
        if re.match(r'[abcdefgh]', file_1) == None:
            return 0
        else:
            return 2

    @staticmethod
    def parse_rank(rank):
        if re.match(r'[1-8]', rank) == None:
            return 0
        else:
            return 1

    @staticmethod
    def parse_castling(castling):
        # matches 00, ooo, o0O, OoO, 0-0-0, etc., but not oo-o, 0-Oo, etc.
        if re.match(r'[oO0]{2,3}|[oO0]\-([oO0]\-)?[oO0]', castling) == None:
            return 0
        else:
            return 128