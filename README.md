![logo-small.png](https://bitbucket.org/repo/g96db7/images/706593193-logo-small.png)

# Cricket Chess #

## Includes ##

* [ cgearhart/Chessnut ](https://github.com/cgearhart/Chessnut)

## Description ##

* Cricket Chess is a console chess app with Unicode figurines and smart movement input.

## How To Use ##

* Cricket Chess is written in pure Python, making play simple. Clone or download the project and run in the Cricket folder: `python imports.py` from the console.
* NOTE: Cricket Chess is still under development and does not work *yet*. Python must also be installed to your PATH environment.

## License ##

* This project is licensed under the Apache License 2.0 (see LICENSE.TXT).

## Future ##

* In the future (not to bash him, but), I would like to replace Chessnut with my own library that holds the game position in FEN notation.
* I would also like to be able to export to pgn format for later retrieval as well.
* I would like to link my program with an AI like Stockfish and have it capable of playing.
* I would like to develop a more advanced GUI, probably in python, but maybe JavaScript.

## Authors ##

* Phillip Benzinger (aka jabberwocky007)

## Contact ##

* Contact this repo's owner for more info.